import gql from 'graphql-tag';

export const Items = gql`
  query($author: String!) {
    items(author: $author) {
      id
      title
      description
      author
    }
  }
`;

export const addItem = gql`
  mutation($author: String!, $title: String!, $description: String!) {
    addItem(input: { title: $title, description: $description, author: $author }) {
      id
      title
      description
    }
  }
`;

export const updateItem = gql`
  mutation($id: ID!, $title: String!, $description: String!) {
    updateItem(input: { id: $id, title: $title, description: $description }, id: $id) {
      id
      title
      description
    }
  }
`;

export const delItem = gql`
  mutation($id: ID!) {
    deleteItem(id: $id) {
      id
    }
  }
`;
