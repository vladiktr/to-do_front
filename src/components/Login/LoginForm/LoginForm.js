import React from "react";
import "./loginForm.css";

const Form = ({setShow, show, emailCheck, signIn, emailErr}) => (
    <div>
        <div className="form__wrap" onClick={() => {setShow(!show)}}></div>
        <form className="form" id="form" onSubmit={emailCheck}>
            <h3>
                Login form
            </h3>
            <p>
                Enter your email:
            </p>
            <input className="form__input" type="text" name="email"></input>
            {emailErr&&
                <div className="form__result">Incorrect email!</div>
            }
            <div className="form__or">or</div>
            <button tupe="button" className="form__google-sing-in form__btn" onClick={signIn}>
                <img src="/img/google-icon.png" alt=""></img> Sign in with Google
                </button>
            <div className="form__buttons">
                <button type="submit" className="form__singIn-btn form__btn">
                    Sing in
                </button>
                
                <button type="button" className="form__close-btn form__btn " onClick={() => {setShow(!show)}}>
                    Close
                </button>
            </div>
        </form>
    </div>
)

export default Form;