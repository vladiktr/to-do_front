import React from "react";
import "./mainHeader.css";

const Header = ({signOut, name}) => (
    <div className="header">
        <div className="header__name">Welcome {name}</div>
        <div className="header__logo">
            <img src="/img/icon.png" alt=""></img>
        </div>
        <div className="header__sign-out" onClick={signOut}>
            <div className="header__button">
                <img src="/img/out.png" alt=""></img>
            </div>
        </div>
    </div>
)

export default Header;