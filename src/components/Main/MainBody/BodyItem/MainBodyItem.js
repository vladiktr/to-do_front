import React, { useState } from 'react';
import './MainBodyItem.css';
import DeleteForm from './deleteForm';

const MainBodyItem = ({ item, items, setItems, setShow, show, setEdit, setTitle, setDescr }) => {
  const [showDel, setShowDel] = useState(false);

  const editItem = () => {
    setShow(!show);
    setEdit(true);

    let id = item.id;
    localStorage.setItem('editId', id);

    Array.prototype.map.call(items, (i) => {
      if (i.id === id) {
        setTitle(i.title);
        setDescr(i.description);
      }
    });
  };

  return (
    <div>
      {showDel ? (
        <DeleteForm
          item={item}
          items={items}
          setItems={setItems}
          setShowDel={setShowDel}
          showDel={showDel}
        />
      ) : null}
      <div className="item__header">
        <button
          className="item__edit-btn item-btns"
          onClick={() => {
            setTitle(item.title);
            setDescr(item.description);
            editItem();
          }}>
          <img src="/img/edit.png" alt=""></img>
        </button>
        <button
          className="item__delete-btn item-btns"
          onClick={() => {
            setShowDel(!showDel);
          }}>
          <img src="/img/delete.png" alt=""></img>
        </button>
      </div>
      <div className="item__title">{item.title}</div>
      <div className="item__descr">{item.description}</div>
    </div>
  );
};

export default MainBodyItem;
