import React from "react";
import "./deleteForm.css";
import { useMutation } from "react-apollo";
import { delItem } from '../../../../graphql/index';

const DeleteForm = ({ setShowDel, showDel, item, items, setItems}) => {

    const [DelItem] = useMutation(delItem);

    function deleteItem() {
        const id = item.id;
        DelItem({
          variables: {
            id: id,
          },
        });
        let newArr = Array.prototype.filter.call(items, i => i.id !== id);
        setItems(newArr);
    }
    
    return(
        <div>
            <div className="AppForm__wrap form__wrap" onClick={() => {setShowDel(!showDel)}}></div>
            <form className="form" >
                <div className="form__title">Delete item</div>
                <div className="form__descr">Are you sure?</div>
                <div className="form__buttons">
                    <button type="button" className="form__add-btn form__btn" onClick={ () => { deleteItem(); setShowDel(!showDel); }}>Confirm</button>
                    <button type="button" className="form__close-btn form__btn" onClick={() => {setShowDel(!showDel)}}>Close</button>
                </div>
            </form>
        </div>
    )
}

export default DeleteForm;