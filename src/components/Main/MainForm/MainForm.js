import React, { useEffect } from 'react';
import './mainForm.css';
import { addItem, updateItem } from '../../../graphql/index';
import { useMutation } from 'react-apollo';

const MainForm = ({
  setFormErr,
  setItems,
  items,
  setShow,
  show,
  formErr,
  edit,
  title,
  descr,
  changingTitle,
  changingDescr,
  setEdit,
  setTitle,
  setDescr,
}) => {
  const [AddItem, { data }] = useMutation(addItem);
  const [UpdateItem] = useMutation(updateItem);
  let AppInputSd;
  let AppInputFs;

  useEffect(() => {
    async function getCards(e) {
      await data;
      if (data) {
        let newArr = [...items, data.addItem];
        setItems(newArr);
        setShow(!show);
      }
    }
    getCards();
  }, [data, show, setEdit, title, descr]);

  return (
    <div>
      <div
        className="AppForm__wrap form__wrap"
        onClick={() => {
          setShow(!show);
        }}></div>
      <form
        className="AppForm form"
        id="AppForm"
        onSubmit={
          edit
            ? async (e) => {
                e.preventDefault();
                if (title === '' || descr === '') {
                  setFormErr(true);
                } else {
                  setFormErr(false);
                  await UpdateItem({
                    variables: {
                      id: localStorage.editId,
                      title: title,
                      description: descr,
                    },
                  });
                  Array.prototype.map.call(items, (i) => {
                    if (i.id === localStorage.editId) {
                      i.title = title;
                      i.description = descr;
                    };
                  });
                  setItems(items);
                  setShow(!show);
                  setEdit(false);
                }
              }
            : async (e) => {
                e.preventDefault();
                if (title === '' || descr === '') {
                  setFormErr(true);
                } else {
                  setFormErr(false);
                  AddItem({
                    variables: {
                      title: AppInputSd.value,
                      description: AppInputFs.value,
                      author: localStorage.getItem('email'),
                    },
                  });
                }
              }
        }>
        <h3>{edit ? 'Edit item' : 'Add item'}</h3>
        <p className="Appform-title">Title:</p>
        <input
          className="AppForm__input form__input AppInputFs"
          type="text"
          name="AppInputFs"
          onChange={changingTitle}
          ref={(node) => {
            AppInputFs = node;
          }}
          value={title}></input>

        <p className="Appform-title">Description:</p>
        <input
          className="AppForm__input form__input"
          type="text"
          name="AppInputSd"
          onChange={changingDescr}
          ref={(node) => {
            AppInputSd = node;
          }}
          value={descr}></input>
        {formErr && <div className="AppForm__result form__result">Fill all Fields</div>}
        <div className="AppForm__buttons form__buttons">
          <button type="submit" className="form__add-btn form__btn">
            Add
          </button>
          <button
            type="button"
            className="form__close-btn form__btn"
            onClick={() => {
              setShow(!show);
            }}>
            Close
          </button>
        </div>
      </form>
    </div>
  );
};

export default MainForm;
