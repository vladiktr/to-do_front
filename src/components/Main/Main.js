import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useQuery } from 'react-apollo';
import Header from './MainHeader/MainHeader';
import Body from './MainBody/MainBody';
import Form from './MainForm/MainForm';

import { Items } from '../../graphql/index.js';

const Main = ({ state, client }) => {
  let history = useHistory();
  const redirect = (URL) => {
    history.push(URL);
  };

  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [formErr, setFormErr] = useState(false);
  const [items, setItems] = useState([]);
  const [title, setTitle] = useState('');
  const [descr, setDescr] = useState('');

  let name = localStorage.getItem('name');
  let email = localStorage.getItem('email');

  const userName = () => {
    name = name !== null ? name : email;
  };
  userName();

  if (localStorage.email === undefined) {
    redirect('/');
  }

  const { data } = useQuery(Items, {
    variables: {
      author: localStorage.getItem('email'),
    },
  });

  function changingTitle(e) {
    const title = e.target.value;
    setTitle(title);
  }

  function changingDescr(e) {
    const descr = e.target.value;
    setDescr(descr);
  }

  useEffect( () => {
    async function getCards(e) {
      await data;
      if (data !== undefined) {
        setItems(data.items);
      }
    }
    getCards();
  }, [data]);

  const signOut = () => {
    state.name = null;
    state.email = null;
    localStorage.clear();
    client.cache.reset().then(() => {
      history.push('/');
    });
  };

  return (
    <div className="app-main">
      <Header signOut={signOut} name={name} email={email} />
      <Body
        show={show}
        setShow={setShow}
        setTitle={setTitle}
        setDescr={setDescr}
        items={items}
        setItems={setItems}
        setEdit={setEdit}
      />
      {show ? (
        <Form
          show={show}
          setShow={setShow}
          title={title}
          descr={descr}
          changingTitle={changingTitle}
          changingDescr={changingDescr}
          formErr={formErr}
          edit={edit}
          setFormErr={setFormErr}
          items={items}
          setItems={setItems}
          setEdit={setEdit}
          setTitle={setTitle}
          setDescr={setDescr}
        />
      ) : null}
    </div>
  );
};

export default Main;
