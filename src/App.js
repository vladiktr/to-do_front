import React from 'react';
import Login from './components/Login/Login';
import Main from './components/Main/Main';
import config from './config/default.json'
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const client = new ApolloClient({
  uri: config.backUrl,
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: null };
    this.state = { name: null };
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <div className="main">
          <Router>
            <Switch>
              <Route render={(props) => <Login state={this.state} />} exact path="/" />
              <Route render={(props) => <Main state={this.state} client={client} />} exact path="/main" />
            </Switch>
          </Router>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
