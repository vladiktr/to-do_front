import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const state = {
  email: null,
  name: null,
}

ReactDOM.render(
  <StrictMode>
    <App state={state} />
  </StrictMode>,
  document.getElementById('root'),
);

reportWebVitals();
